@echo off
:: ========================================================================
:: $File: run.bat $
:: $Date: 2022-10-07 02:39:03 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2022 by Shen, Jen-Chieh $
:: ========================================================================

deno run --allow-net ../webserver.ts
