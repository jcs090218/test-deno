@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2022-12-06 02:01:38 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2022 by Shen, Jen-Chieh $
:: ========================================================================

::deno compile -A ../sandbox.ts
deno compile -A ../yargs.ts
