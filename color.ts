/**
 * $File: color.ts $
 * $Date: 2022-12-22 22:00:36 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2022 by Shen, Jen-Chieh $
 */

import {red, blue, bold} from "https://deno.land/std@0.123.0/fmt/colors.ts";

console.log(red("This text will be printed in red"));
console.log(blue("This text will be printed in blue"));

console.log(bold(red("This text will be red and bold")));
