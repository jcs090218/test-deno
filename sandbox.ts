/**
 * $File: sandbox.ts $
 * $Date: 2022-10-05 23:30:54 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2022 by Shen, Jen-Chieh $
 */
import { serve } from "https://deno.land/std@0.167.0/http/server.ts";

const server = serve({ port: 3000 });
console.log("listening for requests on port 3000");

for await (const req of server) {
    req.respond({ body: `Hello ninjas` });
}

console.log();
