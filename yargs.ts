/**
 * $File: yargs.ts $
 * $Date: 2022-12-22 22:03:14 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2022 by Shen, Jen-Chieh $
 */

//import yargs from 'https://deno.land/x/yargs/deno.ts';
import { Arguments } from 'https://deno.land/x/yargs/deno-types.ts';

yargs(Deno.args)
    .command('download <files...>', 'download a list of files', (yargs: any) => {
        return yargs.positional('files', {
            describe: 'a list of files to do something with'
        })
    }, (argv: any) => {
        console.info(argv)
    })
    .strictCommands()
    .demandCommand(1)
    .parse();
